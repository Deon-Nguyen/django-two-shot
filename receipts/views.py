from django.shortcuts import render, redirect
from receipts.models import Receipt
from django.contrib.auth.decorators import login_required 
from receipts.forms import ReceiptForm, CategoryForm, CreateAccountForm
from receipts.models import ExpenseCategory, Account


@login_required
def my_receipts(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts": receipts
    }
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {
        "form": form,
    }

    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    receipts = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "receipts": receipts
    }
    return render(request, "receipts/categorylist.html", context)


def account_list(request):
    receipts = Account.objects.filter(owner=request.user)
    context = {
        "receipts": receipts
    }
    return render(request, "receipts/accountlist.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = CategoryForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/createcategory.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = CreateAccountForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("account_list")
    else:
        form = CreateAccountForm()
    context = {
        "form": form,
    }

    return render(request, "receipts/createaccount.html", context)